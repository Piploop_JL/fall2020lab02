//Jimmy Le 1936415

package lab2.eclipse;

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public Bicycle(String newManu, int newNumGear, double newMaxSpeed) {
		if(newNumGear < 0 || newMaxSpeed < 0) {
			throw new IllegalArgumentException("Invalid Inputs");
		}
		else {
			manufacturer = newManu;
			numberGears = newNumGear;
			maxSpeed = newMaxSpeed;
		}
	}
	
	public String toString() {
		return ("Manufacturer: " + manufacturer + ", Number of Gears: " + numberGears + ", MaxSpeed: " + maxSpeed);
	}
	
	
	
	public String getManufacturer() {
		return manufacturer;
	}
	
	public int getNumberGears() {
		return numberGears;
	}
	public double getMaxSpeed() {
		return maxSpeed;
	}
}
