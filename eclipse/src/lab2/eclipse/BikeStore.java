//Jimmy Le 1936415
package lab2.eclipse;

public class BikeStore {

	public static void main(String[] args) {
		Bicycle[] store = new Bicycle[4];
		
		store[0] = new Bicycle("Apple", 1, 100);
		store[1] = new Bicycle("Banana", 2, 200);
		store[2] = new Bicycle("Orange", 3, 300);
		store[3] = new Bicycle("Water Melon", 4, 400);
		looper(store);
	}
	
	public static void looper(Bicycle[] store) {
		for(int i = 0; i < store.length; i++) {
			System.out.println(store[i].toString());
		}
	}

}
